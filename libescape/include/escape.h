#ifndef ESCAPE_H
#define ESCAPE_H

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

struct escape_string_view {
	char *start;
	size_t length;
};

struct escape_location {
	int x;
	int y;
};

struct escape_size {
	int w;
	int h;
};

struct escape_mousebtn {
	struct escape_location location;
	int btn;
};

struct escape_embed {
	struct escape_size size;
	struct escape_string_view mime_type;
	struct escape_string_view data;
};

struct escape_color {
	enum {
		ESCAPE_COLOR_NAME,
		ESCAPE_COLOR_RGB,
	} tag;

	union {
		enum {
			ESCAPE_COLOR_NAVY,
			ESCAPE_COLOR_BLUE,
			ESCAPE_COLOR_AQUA,
			ESCAPE_COLOR_TEAL,
			ESCAPE_COLOR_OLIVE,
			ESCAPE_COLOR_GREEN,
			ESCAPE_COLOR_LIME,
			ESCAPE_COLOR_YELLOW,
			ESCAPE_COLOR_ORANGE,
			ESCAPE_COLOR_RED,
			ESCAPE_COLOR_MAROON,
			ESCAPE_COLOR_FUCHSIA,
			ESCAPE_COLOR_PURPLE,
			ESCAPE_COLOR_BLACK,
			ESCAPE_COLOR_GREY,
			ESCAPE_COLOR_SILVER,
			ESCAPE_COLOR_WHITE,
		} name;
		struct {
			unsigned char r;
			unsigned char g;
			unsigned char b;
		} rgb;
	};
};

enum escape_modifier {
	ESCAPE_MODIFIER_CTRL,
	ESCAPE_MODIFIER_META,
	ESCAPE_MODIFIER_ALT,
	ESCAPE_MODIFIER_SHIFT,
};

enum escape_special {
	ESCAPE_SPECIAL_SPACE,
	ESCAPE_SPECIAL_ENTER,
	ESCAPE_SPECIAL_BACKSPACE,
	ESCAPE_SPECIAL_INSERT,
	ESCAPE_SPECIAL_DELETE,
	ESCAPE_SPECIAL_HOME,
	ESCAPE_SPECIAL_END,
	ESCAPE_SPECIAL_PAGEUP,
	ESCAPE_SPECIAL_PAGEDOWN,
	ESCAPE_SPECIAL_LEFT,
	ESCAPE_SPECIAL_RIGHT,
	ESCAPE_SPECIAL_UP,
	ESCAPE_SPECIAL_DOWN,
	ESCAPE_SPECIAL_ESC,
};

struct escape_key {
	enum {
		ESCAPE_KEY_MODIFIER,
		ESCAPE_KEY_SPECIAL,
		ESCAPE_KEY_UNICODE,
	} tag;
	union {
		enum escape_modifier modifier;
		enum escape_special special;
		uint32_t unicode;
	} key;
	enum escape_modifier modifiers[4];
	size_t modifiers_len;
};

enum escape_error {
	ESCAPE_ERROR_FEW_ARGUMENTS,
	ESCAPE_ERROR_INVALID_COLOR,
};

struct escape_sequence_application {
	enum {
		ESCAPE_SEQUENCE_APPLICATION_CTRL_RESET,
		ESCAPE_SEQUENCE_APPLICATION_CTRL_SAFE_PASTE,

		ESCAPE_SEQUENCE_APPLICATION_STYLE_RESET,
		ESCAPE_SEQUENCE_APPLICATION_STYLE_BOLD,
		ESCAPE_SEQUENCE_APPLICATION_STYLE_ITALIC,
		ESCAPE_SEQUENCE_APPLICATION_STYLE_UNDERLINE,
		ESCAPE_SEQUENCE_APPLICATION_STYLE_FG,
		ESCAPE_SEQUENCE_APPLICATION_STYLE_BG,

		ESCAPE_SEQUENCE_APPLICATION_EMBED_INLINE,
		ESCAPE_SEQUENCE_APPLICATION_EMBED_BLOCK,

		ESCAPE_SEQUENCE_APPLICATION_QUERY_EMBED,
		ESCAPE_SEQUENCE_APPLICATION_QUERY_SIZE,

		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESET,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEMOVE,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSECLICK,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEDOWN,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEUP,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESIZE,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYDOWN,
		ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYUP,

		ESCAPE_SEQUENCE_APPLICATION_UNKNOWN,
		ESCAPE_SEQUENCE_APPLICATION_ERROR,
	} tag;

	union {
		struct escape_color color; // For STYLE_FG and STYLE_BG
		struct escape_embed embed; // For EMBED_*
		struct escape_string_view mime_type; // For QUERY_EMBED
		enum escape_error error; // For ERROR
	};
};

struct escape_sequence_terminal {
	enum {
		ESCAPE_SEQUENCE_TERMINAL_KEY,

		ESCAPE_SEQUENCE_TERMINAL_EVENT_MOUSEMOVE,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_MOUSECLICK,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_MOUSEDOWN,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_MOUSEUP,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_RESIZE,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_KEYDOWN,
		ESCAPE_SEQUENCE_TERMINAL_EVENT_KEYUP,

		ESCAPE_SEQUENCE_TERMINAL_QUERY_RESPONSE_EMBED,
		ESCAPE_SEQUENCE_TERMINAL_QUERY_RESPONSE_SIZE,

		ESCAPE_SEQUENCE_TERMINAL_UNKNOWN,
		ESCAPE_SEQUENCE_TERMINAL_ERROR,
	} tag;

	union {
		struct escape_key key; // For EVENT_KEY, EVENT_KEYDOWN, EVENT_KEYUP
		struct escape_location location; // For EVENT_MOUSEMOVE
		struct escape_mousebtn button; // For EVENT_MOUSE{CLICK,DOWN,UP}
		struct escape_size size; // For EVENT_RESIZE
		enum escape_error error; // For ERROR
	};
};

/*
 * Parse an application escape sequence.
 * This function might reference 'buf' through an escape_string_view struct,
 * so the string views should only be considered valid as long as 'buf' is valid.
 *
 * Args:
 *     esc: An escape_sequence_application struct which will be filled with data.
 *     buf: The buffer containing a serialized escape sequence.
 *     buflen: The length of 'buf'.
 *
 * Returns:
 *     The amount of bytes read from 'buf', or 0 if 'buf' doesn't start with
 *     an escape sequence.
 */
size_t escape_sequence_application_parse(
		struct escape_sequence_application *esc, char *buf, size_t buflen);

/*
 * Serialize an application escape sequence.
 *
 * Args:
 *     esc: The escape_sequence_application struct to be serialized.
 *     buf: The buffer which will be filled with dat.
 *     buflen: The length of 'buf'.
 *
 * Returns:
 *     If the buffer is large enough to contain the serialized escape sequence,
 *     the number of bytes used will be returned.
 *     If the buffer is too small, the amount of bytes which would be needed
 *     to serialize the escape sequence is returned; this number will always
 *     be bigger than 'buflen'.
 */
size_t escape_sequence_application_serialize(
		struct escape_sequence_application *esc, char *buf, size_t buflen);

/*
 * Parse a terminal escape sequence.
 * This function might reference 'buf' through an escape_string_view struct,
 * so the string views should only be considered valid as long as 'buf' is valid.
 *
 * Args:
 *     esc: An escape_sequence_terminal struct which will be filled with data.
 *     buf: The buffer containing a serialized escape sequence.
 *     buflen: The length of 'buf'.
 *
 * Returns:
 *     The amount of bytes read from 'buf', or 0 if 'buf' doesn't start with
 *     an escape sequence.
 */
size_t escape_sequence_terminal_parse(
		struct escape_sequence_terminal *esc, char *buf, size_t buflen);

/*
 * Serialize a terminal escape sequence.
 *
 * Args:
 *     esc: The escape_sequence_terminal struct to be serialized.
 *     buf: The buffer which will be filled with dat.
 *     buflen: The length of 'buf'.
 *
 * Returns:
 *     If the buffer is large enough to contain the serialized escape sequence,
 *     the number of bytes used will be returned.
 *     If the buffer is too small, the amount of bytes which would be needed
 *     to serialize the escape sequence is returned; this number will always
 *     be bigger than 'buflen'.
 */
size_t escape_sequence_terminal_serialize(
		struct escape_sequence_terminal *esc, char *buf, size_t buflen);

#endif
