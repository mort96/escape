# Protocol

* An escape sequence is a sequence of bytes which starts with `\e[` (0x1B 0x5B),
  and ends with `~` (0x7E).
* Escape sequences which aren't understood should be ignored
  (though an error may be logged if appropriate).
* Any byte sequence which doesn't start with `\e[` (0x1B 0x5B) is a UTF-8 encoded
  unicode code point.
* Due to the relative verbosity of this protocol, it might be desirable to use
  gzip over slow connections such as serial or a network, though that is out of
  the scope of this protocol.

## Concepts

* `<x>`: A location; the number of rows from the left edge, starting at 0.
* `<y>`: A location; the number of columns from the top edge, starting at 0.
* `<w>`: A width, in columns.
* `<h>`: A height, in columns.
* `<bool>`: Either `true` or `false`.
* `<btn>`: A mouse button. 1 is right click, 2 is middle click, 3 is right
  click.
* `<modifier>`: A modifier key.
	* `Ctrl`, `Meta`, `Alt`, `Shift`
* `<modifiers>`: A comma-separated list of `<modifier>`s, or empty.
* `<key>`: A key. Can be any of:
	* A `<modifier>`
	* `Space`, `Enter`, `Backspace`, `Insert`, `Delete`, `Home`, `End`,
	  `PageUp`, `PageDown`, `Left`, `Right`, `Up`, `Down`, `Esc`,
	  `F1` through `F12`.
	* If none of the above matches,
	  `u<1-8 hex digits`: Any unicode code point encoded as hex.
* `<color>`:
	* `#<rgb>`
	* `#<rrggbb>`
	* `navy`, `blue`, `aqua`, `teal`, `olive`, `green`, `lime`, `yellow`,
	  `orange`, `red`, `maroon`, `fuchsia`, `purple`, `black`, `grey`,
	  `silver`, `white` (suggested defaults: https://clrs.cc/)

## Application -> Terminal

### Control

* `\e[ctrl:reset~`: Reset all state (styling, event listeners, etc.) to the
  default state. This can, for example, be used by shells when they spawn child
  processes.
* `\e[ctrl:safe-paste~`: Indicate to the terminal that the client supports the
  `paste` escape sequence.

### Style

* `\e[style:reset~`: Reset formatting to default.
* `\e[style:bold~`: Make text bold until `reset`.
* `\e[style:italic~`: Make text italic until `reset`.
* `\e[style:underline~`: Make text underlined until `reset`.
* `\e[style:fg:<color>~`: Make foreground text `<color>` until reset or overwritten
  by another `fg`.
* `\e[style:bg:<color>~`: Make the background `<color>` until reset or overwritten
  by another `bg`.

### Embed

* `\e[embed:inline:<w>:<h>:<mime-type>:<base64-encoded data>~`: Embed an object (such as an
  image) in line with the rest of the content.
* `\e[embed:block:<w>:<h>:<mime-type>:<base64-encoded data>~`: Embed an object as a
  block element.
* `<w>` is a width in columns, and `<h>` is a height in rows. Either can be
  `auto`:
	* If one of `<w>` or `<h>` is `auto`, and the object has an aspect ratio, the
	  aspect ratio should be preserved.
	* If one of `<w>` or `<h>` is `auto`, and the object doesn't have an aspect
	  ratio, the object should be rendered as a square.

### Query

See the "Query responses" section under "Terminal -> Application".

* `\e[query:embed:<mime-type>~`: Query whether emdedding an object with the
  mime type `<mime-type>` is supported.
* `\e[query:size~`: Query the size of the terminal.

## Subscribe to events

See the "Events" section under "Terminal -> Application".

* `\e[subscribe:reset~`: Reset all event subscriptions.
* `\e[subscribe:mousemove~`: Subscribe to mouse move events.
* `\e[subscribe:mouseclick~`: Subscribe to mouse click events.
* `\e[subscribe:mousedown~`: Subscribe to mousedown events.
* `\e[subscribe:mouseup~`: Subscribe to mouseup events.
* `\e[subscribe:resize~`: Subscribe to resize events.
* `\e[subscribe:keydown~`: Subscribe to keydown events.
* `\e[subscribe:keyup~`: Subscribe to keyup events.

## Terminal -> Application

### Keys

* `\e[key:<key>:<modifiers>~`: A key with optional modifiers.
  Note that regular keys without any modifiers, or regular keys with only the
  "Shift" modifier, should just be sent as the utf-8 sequence for the
  corresponding unicode character, not as a `key` escape sequence.

### Events

* `\e[event:mousemove:<x>:<y>~`: The mouse was moved to column `<x>`, row `<y>`.
* `\e[event:mouseclick:<x>:<y>:<btn>~`: Button `<btn>` was clicked.
* `\e[event:mousedown:<x>:<y>:<btn>~`: Button `<btn>` was pressed.
* `\e[event:mouseup:<x>:<y>:<btn>~`: Button `<btn>` was released.
* `\e[event:resize:<w>:<h>~`: The terminal was resized.
* `\e[event:keydown:<key>~`: The key `<key>` was pressed.
* `\e[event:keyup:<key>~`: The key `<key>` was released.

### Query responses

* `\e[query-response:embed:<mime-type>:<bool>~`: Whether the mime-type is
  supported or not.
* `\e[query-response:size:<w>:<h>~`: The terminal is `<w>x<h>`.
