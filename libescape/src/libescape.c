#include <escape.h>

#include <string.h>

#include <stdio.h>

#define streq(strv, str) \
	(strncmp((strv)->start, str, (strv)->length) == 0)

static unsigned char hexval1(char x) {
	if (x >= '0' && x <= '9')
		return x - '0';
	else if (x == 'a' || x == 'A')
		return 10;
	else if (x == 'b' || x == 'B')
		return 11;
	else if (x == 'c' || x == 'C')
		return 12;
	else if (x == 'd' || x == 'D')
		return 13;
	else if (x == 'e' || x == 'E')
		return 14;
	else if (x == 'f' || x == 'F')
		return 15;
	else
		return 0;
}

static uint32_t hexval(char *str, size_t len) {
	uint32_t val = 0;

	for (size_t i = 0; i < len; ++i) {
		val |= hexval1(str[i]) << (len - i - 1) * 4;
	}

	return val;
}

static int parse_color(
		struct escape_color *color, struct escape_string_view *strv) {
	if (strv->start[0] == '#') {
		color->tag = ESCAPE_COLOR_RGB;
		if (strv->length == strlen("#XXX")) {
			color->rgb.r = hexval((char[]) { strv->start[1], strv->start[1] }, 2);
			color->rgb.g = hexval((char[]) { strv->start[2], strv->start[2] }, 2);
			color->rgb.b = hexval((char[]) { strv->start[3], strv->start[3] }, 2);
		} else if (strv->length == strlen("#XXXXXX")) {
			color->rgb.r = hexval(strv->start + 1, 2);
			color->rgb.g = hexval(strv->start + 3, 2);
			color->rgb.b = hexval(strv->start + 5, 2);
		} else {
			return -1;
		}
	} else {
		color->tag = ESCAPE_COLOR_NAME;
		if (streq(strv, "navy")) {
			color->name = ESCAPE_COLOR_NAVY;
		} else if (streq(strv, "blue")) {
			color->name = ESCAPE_COLOR_BLUE;
		} else if (streq(strv, "aqua")) {
			color->name = ESCAPE_COLOR_AQUA;
		} else if (streq(strv, "teal")) {
			color->name = ESCAPE_COLOR_TEAL;
		} else if (streq(strv, "olive")) {
			color->name = ESCAPE_COLOR_OLIVE;
		} else if (streq(strv, "green")) {
			color->name = ESCAPE_COLOR_GREEN;
		} else if (streq(strv, "lime")) {
			color->name = ESCAPE_COLOR_LIME;
		} else if (streq(strv, "yellow")) {
			color->name = ESCAPE_COLOR_YELLOW;
		} else if (streq(strv, "orange")) {
			color->name = ESCAPE_COLOR_ORANGE;
		} else if (streq(strv, "red")) {
			color->name = ESCAPE_COLOR_RED;
		} else if (streq(strv, "maroon")) {
			color->name = ESCAPE_COLOR_MAROON;
		} else if (streq(strv, "fuchsia")) {
			color->name = ESCAPE_COLOR_FUCHSIA;
		} else if (streq(strv, "purple")) {
			color->name = ESCAPE_COLOR_PURPLE;
		} else if (streq(strv, "black")) {
			color->name = ESCAPE_COLOR_BLACK;
		} else if (streq(strv, "grey")) {
			color->name = ESCAPE_COLOR_GREY;
		} else if (streq(strv, "silver")) {
			color->name = ESCAPE_COLOR_SILVER;
		} else if (streq(strv, "white")) {
			color->name = ESCAPE_COLOR_WHITE;
		} else {
			return -1;
		}
	}

	return 0;
}

static int parse_modifier(
		enum escape_modifier *modifier, struct escape_string_view *strv) {
	if (streq(strv, "Ctrl")) {
		*modifier = ESCAPE_MODIFIER_CTRL;
	} else if (streq(strv, "Meta")) {
		*modifier = ESCAPE_MODIFIER_META;
	} else if (streq(strv, "Alt")) {
		*modifier = ESCAPE_MODIFIER_ALT;
	} else if (streq(strv, "Shift")) {
		*modifier = ESCAPE_MODIFIER_SHIFT;
	} else {
		return -1;
	}

	return 0;
}

static int parse_special(
		enum escape_special *special, struct escape_string_view *strv) {
	if (streq(strv, "Space")) {
		*special = ESCAPE_SPECIAL_SPACE;
	} else if (streq(strv, "Enter")) {
		*special = ESCAPE_SPECIAL_ENTER;
	} else if (streq(strv, "Backspace")) {
		*special = ESCAPE_SPECIAL_BACKSPACE;
	} else if (streq(strv, "Insert")) {
		*special = ESCAPE_SPECIAL_INSERT;
	} else if (streq(strv, "Delete")) {
		*special = ESCAPE_SPECIAL_DELETE;
	} else if (streq(strv, "Home")) {
		*special = ESCAPE_SPECIAL_HOME;
	} else if (streq(strv, "End")) {
		*special = ESCAPE_SPECIAL_END;
	} else if (streq(strv, "PageUp")) {
		*special = ESCAPE_SPECIAL_PAGEUP;
	} else if (streq(strv, "PageDown")) {
		*special = ESCAPE_SPECIAL_PAGEDOWN;
	} else if (streq(strv, "Left")) {
		*special = ESCAPE_SPECIAL_LEFT;
	} else if (streq(strv, "Right")) {
		*special = ESCAPE_SPECIAL_RIGHT;
	} else if (streq(strv, "Up")) {
		*special = ESCAPE_SPECIAL_UP;
	} else if (streq(strv, "Down")) {
		*special = ESCAPE_SPECIAL_DOWN;
	} else if (streq(strv, "Esc")) {
		*special = ESCAPE_SPECIAL_ESC;
	} else {
		return -1;
	}

	return 0;
}

static void parse_modifiers(
		enum escape_modifier *modifiers, size_t *modifiers_len,
		struct escape_string_view *strv) {
	struct escape_string_view current;
	current.start = strv->start;

	size_t maxlen = *modifiers_len;
	*modifiers_len = 0;

	for (size_t i = 0; i < strv->length; ++i) {
		if (i == strv->length - 1 || strv->start[i + 1] == ',') {
			current.length = i - (current.start - strv->start) + 1;

			if (streq(&current, "Ctrl"))
				modifiers[(*modifiers_len)++] = ESCAPE_MODIFIER_CTRL;
			else if (streq(&current, "Meta"))
				modifiers[(*modifiers_len)++] = ESCAPE_MODIFIER_META;
			else if (streq(&current, "Alt"))
				modifiers[(*modifiers_len)++] = ESCAPE_MODIFIER_ALT;
			else if (streq(&current, "Shift"))
				modifiers[(*modifiers_len)++] = ESCAPE_MODIFIER_SHIFT;

			if (*modifiers_len >= maxlen)
				break;

			current.start = strv->start + i + 2;
		}
	}
}

static int parse_key(
		struct escape_key *key, struct escape_string_view *strv) {
	if (strv->length >= 2 && strv->length <= 9 && strv->start[0] == 'u') {
		key->tag = ESCAPE_KEY_UNICODE;
		key->key.unicode = hexval(strv->start + 1, strv->length - 1);
	} else if (parse_modifier(&key->key.modifier, strv) >= 0) {
		key->tag = ESCAPE_KEY_MODIFIER;
	} else if (parse_special(&key->key.special, strv) >= 0) {
		key->tag = ESCAPE_KEY_SPECIAL;
	} else {
		return -1;
	}

	return 0;
}

static size_t parse_sequence(
		struct escape_string_view *strs, size_t *strs_len,
		char *buf, size_t buflen) {
	if (buflen < 3)
		return 0;
	if (buf[0] != '\033')
		return 0;
	if (buf[1] != '[')
		return 0;

	size_t maxlen = *strs_len;
	*strs_len = 0;
	strs[0].start = buf + 2;

	size_t esclen = 0;
	for (size_t i = 0; i < buflen; ++i) {
		if (*strs_len < maxlen && (buf[i] == '~' || buf[i] == ':')) {
			strs[*strs_len].length = buf + i - strs[*strs_len].start;
			if (buf[i] == ':')
				strs[*strs_len + 1].start = buf + i + 1;
			*strs_len += 1;
		}

		if (buf[i] == '~') {
			esclen = i + 1;
			break;
		}
	}

	return esclen;
}

size_t escape_sequence_application_parse(
		struct escape_sequence_application *esc, char *buf, size_t buflen) {

	struct escape_string_view strs[8];
	size_t strs_len = 8;
	size_t esclen = parse_sequence(strs, &strs_len, buf, buflen);
	if (esclen == 0)
		return 0;

	if (strs_len < 2) {
		esc->error =  ESCAPE_ERROR_FEW_ARGUMENTS;
		esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
		return esclen;
	}

	esc->tag = ESCAPE_SEQUENCE_APPLICATION_UNKNOWN;
	if (streq(&strs[0], "ctrl")) {
		if (streq(&strs[1], "reset")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_CTRL_RESET;
		} else if (streq(&strs[1], "safe-paste")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_CTRL_SAFE_PASTE;
		}
	} else if (streq(&strs[0], "style")) {
		if (streq(&strs[1], "reset")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_RESET;
		} else if (streq(&strs[1], "bold")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_BOLD;
		} else if (streq(&strs[1], "italic")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_ITALIC;
		} else if (streq(&strs[1], "underline")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_UNDERLINE;
		} else if (streq(&strs[1], "fg") || streq(&strs[1], "bg")) {
			if (strs_len < 3) {
				esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
				esc->error = ESCAPE_ERROR_FEW_ARGUMENTS;
				return esclen;
			}

			if (parse_color(&esc->color, &strs[2]) < 0) {
				esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
				esc->error = ESCAPE_ERROR_INVALID_COLOR;
			} else if (streq(&strs[1], "fg")) {
				esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_FG;
			} else {
				esc->tag = ESCAPE_SEQUENCE_APPLICATION_STYLE_BG;
			}
		}
	} else if (streq(&strs[0], "embed")) {
		if (strs_len < 6) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
			esc->error = ESCAPE_ERROR_FEW_ARGUMENTS;
			return esclen;
		}

		if (streq(&strs[1], "inline")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_EMBED_INLINE;
		} else if (streq(&strs[1], "block")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_EMBED_BLOCK;
		} else {
			return esclen;
		}

		if (streq(&strs[2], "auto"))
			esc->embed.size.w = -1;
		else
			esc->embed.size.w = strtol(strs[2].start, NULL, 10);

		if (streq(&strs[3], "auto"))
			esc->embed.size.h = -1;
		else
			esc->embed.size.h = strtol(strs[3].start, NULL, 10);

		esc->embed.mime_type = strs[4];
		esc->embed.data = strs[5];
	} else if (streq(&strs[0], "query")) {
		if (streq(&strs[1], "embed")) {
			if (strs_len < 3) {
				esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
				esc->error = ESCAPE_ERROR_FEW_ARGUMENTS;
				return esclen;
			}

			esc->tag = ESCAPE_SEQUENCE_APPLICATION_QUERY_EMBED;
			esc->mime_type = strs[2];
		} else if (streq(&strs[1], "size")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_QUERY_SIZE;
		}
	} else if (streq(&strs[0], "subscribe")) {
		if (streq(&strs[1], "reset")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESET;
		} else if (streq(&strs[1], "mousemove")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEMOVE;
		} else if (streq(&strs[1], "mouseclick")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSECLICK;
		} else if (streq(&strs[1], "mousedown")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEDOWN;
		} else if (streq(&strs[1], "mouseup")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEUP;
		} else if (streq(&strs[1], "resize")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESIZE;
		} else if (streq(&strs[1], "keydown")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYDOWN;
		} else if (streq(&strs[1], "keyup")) {
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYUP;
		}
	}

	return esclen;
}

size_t escape_sequence_terminal_parse(
		struct escape_sequence_terminal *esc, char *buf, size_t buflen) {

	memset(esc, 0, sizeof(*esc));

	struct escape_string_view strs[8];
	size_t strs_len = 8;
	size_t esclen = parse_sequence(strs, &strs_len, buf, buflen);
	if (esclen == 0)
		return 0;

	if (strs_len < 2) {
		esc->error =  ESCAPE_ERROR_FEW_ARGUMENTS;
		esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
		return esclen;
	}

	esc->tag = ESCAPE_SEQUENCE_TERMINAL_UNKNOWN;
	if (streq(&strs[0], "key")) {
		if (strs_len < 3) {
			esc->error =  ESCAPE_ERROR_FEW_ARGUMENTS;
			esc->tag = ESCAPE_SEQUENCE_APPLICATION_ERROR;
			return esclen;
		}

		if (parse_key(&esc->key, &strs[1]) < 0)
			return esclen;

		esc->key.modifiers_len = 4;
		parse_modifiers(esc->key.modifiers, &esc->key.modifiers_len, &strs[2]);
		esc->tag = ESCAPE_SEQUENCE_TERMINAL_KEY;
	}

	return esclen;
}
