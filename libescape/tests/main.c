#include <snow/snow.h>
#include <escape.h>

#define streq(strv, str) \
	(strncmp((strv)->start, str, (strv)->length) == 0)

describe(escape_sequence_application_parse) {
	struct escape_sequence_application esc;

#define parse_application_esc(str) do { \
	char buf[] = str; \
	asserteq( \
		escape_sequence_application_parse( \
			&esc, buf, sizeof(buf) - 1), \
		sizeof(buf) - 1); \
} while (0)

	it("doesn't parse non-escape sequences") {
		char buf1[] = "no"; // short string
		asserteq(
			escape_sequence_application_parse(
				&esc, buf1, sizeof(buf1) - 1),
			0);
		char buf2[] = "hello world"; // long-ish string
		asserteq(
			escape_sequence_application_parse(
				&esc, buf2, sizeof(buf2) - 1),
			0);
		char buf3[] = "\033no"; // non-escape sequence starting with \e
		asserteq(
			escape_sequence_application_parse(
				&esc, buf3, sizeof(buf3) - 1),
			0);
	}

	subdesc(ctrl) {
		it("parses ctrl:reset") {
			parse_application_esc("\033[ctrl:reset~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_CTRL_RESET);
		}

		it("parses ctrl:safe-paste") {
			parse_application_esc("\033[ctrl:safe-paste~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_CTRL_SAFE_PASTE);
		}
	}

	subdesc(style) {
		it("parses style:reset") {
			parse_application_esc("\033[style:reset~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_RESET);
		}

		it("parses style:bold") {
			parse_application_esc("\033[style:bold~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BOLD);
		}

		it("parses style:italic") {
			parse_application_esc("\033[style:italic~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_ITALIC);
		}

		it("parses style:underline") {
			parse_application_esc("\033[style:underline~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_UNDERLINE);
		}

		it("parses style:fg with color names") {
			parse_application_esc("\033[style:fg:navy~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_NAVY);

			parse_application_esc("\033[style:fg:blue~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_BLUE);

			parse_application_esc("\033[style:fg:fuchsia~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_FUCHSIA);
		}

		it("parses style:fg with hex values") {
			parse_application_esc("\033[style:fg:#5a3~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0x55);
			asserteq(esc.color.rgb.g, 0xaa);
			asserteq(esc.color.rgb.b, 0x33);

			parse_application_esc("\033[style:fg:#000~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0x00);
			asserteq(esc.color.rgb.g, 0x00);
			asserteq(esc.color.rgb.b, 0x00);

			parse_application_esc("\033[style:fg:#fFf~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0xff);
			asserteq(esc.color.rgb.g, 0xff);
			asserteq(esc.color.rgb.b, 0xff);

			parse_application_esc("\033[style:fg:#a4F386~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_FG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0xa4);
			asserteq(esc.color.rgb.g, 0xf3);
			asserteq(esc.color.rgb.b, 0x86);
		}

		it("parses style:bg with color names") {
			parse_application_esc("\033[style:bg:navy~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_NAVY);

			parse_application_esc("\033[style:bg:blue~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_BLUE);

			parse_application_esc("\033[style:bg:fuchsia~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_NAME);
			asserteq(esc.color.name, ESCAPE_COLOR_FUCHSIA);
		}

		it("parses style:bg with hex values") {
			parse_application_esc("\033[style:bg:#5a3~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0x55);
			asserteq(esc.color.rgb.g, 0xaa);
			asserteq(esc.color.rgb.b, 0x33);

			parse_application_esc("\033[style:bg:#000~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0x00);
			asserteq(esc.color.rgb.g, 0x00);
			asserteq(esc.color.rgb.b, 0x00);

			parse_application_esc("\033[style:bg:#fFf~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0xff);
			asserteq(esc.color.rgb.g, 0xff);
			asserteq(esc.color.rgb.b, 0xff);

			parse_application_esc("\033[style:bg:#a4F386~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_STYLE_BG);
			asserteq(esc.color.tag, ESCAPE_COLOR_RGB);
			asserteq(esc.color.rgb.r, 0xa4);
			asserteq(esc.color.rgb.g, 0xf3);
			asserteq(esc.color.rgb.b, 0x86);
		}
	}

	subdesc(embed) {
		it("parses embed:inline") {
			parse_application_esc(
				"\033[embed:inline:10:10:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_INLINE);
			asserteq(esc.embed.size.w, 10);
			asserteq(esc.embed.size.h, 10);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));
		}

		it("parses embed:inline with auto values") {
			parse_application_esc(
				"\033[embed:inline:10:auto:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_INLINE);
			asserteq(esc.embed.size.w, 10);
			asserteq(esc.embed.size.h, -1);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));

			parse_application_esc(
				"\033[embed:inline:auto:10:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_INLINE);
			asserteq(esc.embed.size.w, -1);
			asserteq(esc.embed.size.h, 10);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));
		}

		it("parses embed:block") {
			parse_application_esc(
				"\033[embed:block:10:10:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_BLOCK);
			asserteq(esc.embed.size.w, 10);
			asserteq(esc.embed.size.h, 10);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));
		}

		it("parses embed:block with auto values") {
			parse_application_esc(
				"\033[embed:block:10:auto:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_BLOCK);
			asserteq(esc.embed.size.w, 10);
			asserteq(esc.embed.size.h, -1);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));

			parse_application_esc(
				"\033[embed:block:auto:10:image/jpeg:"
				"081ecc5e6dd6ba0d150fc4bc0e62ec50~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_EMBED_BLOCK);
			asserteq(esc.embed.size.w, -1);
			asserteq(esc.embed.size.h, 10);
			assert(streq(&esc.embed.mime_type, "image/jpeg"));
			assert(streq(&esc.embed.data, "081ecc5e6dd6ba0d150fc4bc0e62ec50"));
		}
	}

	subdesc(query) {
		it("parses query:embed") {
			parse_application_esc("\033[query:embed:image/jpeg~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_QUERY_EMBED);
			assert(streq(&esc.mime_type, "image/jpeg"));
		}

		it("parses query:size") {
			parse_application_esc("\033[query:size~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_QUERY_SIZE);
		}
	}

	subdesc(subscribe) {
		it("parses subscribe:reset") {
			parse_application_esc("\033[subscribe:reset~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESET);
		}

		it("parses subscribe:mousemove") {
			parse_application_esc("\033[subscribe:mousemove~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEMOVE);
		}

		it("parses subscribe:mouseclick") {
			parse_application_esc("\033[subscribe:mouseclick~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSECLICK);
		}

		it("parses subscribe:mousedown") {
			parse_application_esc("\033[subscribe:mousedown~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEDOWN);
		}

		it("parses subscribe:mouseup") {
			parse_application_esc("\033[subscribe:mouseup~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_MOUSEUP);
		}

		it("parses subscribe:resize") {
			parse_application_esc("\033[subscribe:resize~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_RESIZE);
		}

		it("parses subscribe:keydown") {
			parse_application_esc("\033[subscribe:keydown~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYDOWN);
		}

		it("parses subscribe:keyup") {
			parse_application_esc("\033[subscribe:keyup~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_APPLICATION_SUBSCRIBE_KEYUP);
		}
	}
}

describe(escape_sequence_terminal_parse) {
	struct escape_sequence_terminal esc;

#define parse_terminal_esc(str) do { \
	char buf[] = str; \
	asserteq( \
		escape_sequence_terminal_parse( \
			&esc, buf, sizeof(buf) - 1), \
		sizeof(buf) - 1); \
} while (0)

	subdesc(key) {
		it("parses modifier key presses") {
			parse_terminal_esc("\033[key:Ctrl:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_MODIFIER);
			asserteq(esc.key.key.modifier, ESCAPE_MODIFIER_CTRL);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:Meta:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_MODIFIER);
			asserteq(esc.key.key.modifier, ESCAPE_MODIFIER_META);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:Alt:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_MODIFIER);
			asserteq(esc.key.key.modifier, ESCAPE_MODIFIER_ALT);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:Shift:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_MODIFIER);
			asserteq(esc.key.key.modifier, ESCAPE_MODIFIER_SHIFT);
			asserteq(esc.key.modifiers_len, 0);
		}

		it("parses special key presses") {
			parse_terminal_esc("\033[key:Space:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_SPECIAL);
			asserteq(esc.key.key.special, ESCAPE_SPECIAL_SPACE);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:Enter:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_SPECIAL);
			asserteq(esc.key.key.special, ESCAPE_SPECIAL_ENTER);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:PageUp:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_SPECIAL);
			asserteq(esc.key.key.special, ESCAPE_SPECIAL_PAGEUP);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:Esc:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_SPECIAL);
			asserteq(esc.key.key.special, ESCAPE_SPECIAL_ESC);
			asserteq(esc.key.modifiers_len, 0);
		}

		it("parses utf-8 key presses") {
			parse_terminal_esc("\033[key:uc:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0xc);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u3A:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x3a);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u3A8:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x3a8);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u4c7a:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x4c7a);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u4c7a:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x4c7a);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u1f4a9:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x1f4a9);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u1f4a9c:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x1f4a9c);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u1f4a9c3:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x1f4a9c3);
			asserteq(esc.key.modifiers_len, 0);

			parse_terminal_esc("\033[key:u1f4a9c31:~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x1f4a9c31);
			asserteq(esc.key.modifiers_len, 0);
		}

		it("parses key presses with modifiers") {
			parse_terminal_esc("\033[key:Ctrl:Shift~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_MODIFIER);
			asserteq(esc.key.key.modifier, ESCAPE_MODIFIER_CTRL);
			asserteq(esc.key.modifiers_len, 1);
			asserteq(esc.key.modifiers[0], ESCAPE_MODIFIER_SHIFT);

			parse_terminal_esc("\033[key:Enter:Shift,Ctrl~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_SPECIAL);
			asserteq(esc.key.key.modifier, ESCAPE_SPECIAL_ENTER);
			asserteq(esc.key.modifiers_len, 2);
			asserteq(esc.key.modifiers[0], ESCAPE_MODIFIER_SHIFT);
			asserteq(esc.key.modifiers[1], ESCAPE_MODIFIER_CTRL);

			parse_terminal_esc("\033[key:u1f4a9:Shift,Ctrl,Alt,Meta~");
			asserteq(esc.tag, ESCAPE_SEQUENCE_TERMINAL_KEY);
			asserteq(esc.key.tag, ESCAPE_KEY_UNICODE);
			asserteq(esc.key.key.unicode, 0x1f4a9);
			asserteq(esc.key.modifiers_len, 4);
			asserteq(esc.key.modifiers[0], ESCAPE_MODIFIER_SHIFT);
			asserteq(esc.key.modifiers[1], ESCAPE_MODIFIER_CTRL);
			asserteq(esc.key.modifiers[2], ESCAPE_MODIFIER_ALT);
			asserteq(esc.key.modifiers[3], ESCAPE_MODIFIER_META);
		}
	}
}

snow_main();
